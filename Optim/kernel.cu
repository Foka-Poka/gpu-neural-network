
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math_functions.h"
#include "math_constants.h"
#include "cuda.h"
#include "curand.h"

#include <stdio.h>

#define SIZE 50*1024*1024

struct Record
{
	int threadCount;
	int blockCount;
	float elapsedTime;

	bool operator == (const Record &r)
	{
		return this->elapsedTime == r.elapsedTime;
	}

	bool operator < (const Record &r)
	{
		return this->elapsedTime < r.elapsedTime;
	}

	bool operator > (const Record &r)
	{
		return this->elapsedTime > r.elapsedTime;
	}
};

void GPUFillRand(float *a, bool ignore = false) // using standarnt cuRand
{
	curandGenerator_t gen;
	float *dev_data;
	long long param = rand();

	cudaEvent_t start, stop;
	float elapsedTime;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);


	cudaMalloc((void**)&dev_data, SIZE * sizeof(float));

	cudaEventRecord(start, 0);

	curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen, param);
	curandGenerateUniform(gen, dev_data, SIZE);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);

	cudaMemcpy(a, dev_data, SIZE * sizeof(float), cudaMemcpyDeviceToHost);

	if (!ignore)
		printf("\n Generating random GPU time = %.1f \n", elapsedTime);

	curandDestroyGenerator(gen);
	cudaFree(dev_data);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	return;
}

__global__ void GPUOutFunc(float *a, float *c)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;
	float t = 0.5;
	t = -t;

	while (ind < SIZE)
	{
		float expRes = expf(t*a[ind]);
		float denum = 1 + expRes;
		float sigma = 1 / denum;
		//float k = fdividef(1., t);
		c[ind] = sigma;
		ind += round;
	}

}

void optimOutFunc()
{
	Record bestRes;

	bestRes.elapsedTime = 99999;

	FILE *resFile;
	/*resFile = fopen("E:\\matrix\\UIR\\outFuncOptim.txt", "r");

	int thread = 1;
	int block = 1;
	float time = 0;

	while (!feof(resFile))
	{
		fscanf(resFile, "%d", &block);
		fscanf(resFile, "%d", &thread);
		fscanf(resFile, "%f", &time);

		if (bestRes.elapsedTime > time)
		{
			bestRes.elapsedTime = time;
			bestRes.blockCount = block;
			bestRes.threadCount = thread;
		}

	}
	*/
	int lastBlock = 1;// block;
	int lastThread = 1;// thread;
	//fclose(resFile);

	float *a, *b, *c, *copy_dev_c;
	float *dev_a, *dev_b, *dev_c;

	a = (float *)malloc(SIZE * sizeof(float));
	c = (float *)malloc(SIZE * sizeof(float));
	copy_dev_c = (float *)malloc(SIZE * sizeof(float));

	cudaMalloc((void **)&dev_a, SIZE*sizeof(float));
	cudaMalloc((void **)&dev_c, SIZE*sizeof(float));

	GPUFillRand(a, true);
	cudaMemcpy(dev_a, a, SIZE * sizeof(float), cudaMemcpyHostToDevice);

	for (int i = 1; i < 65535; ++i)
	{
		int j = 1;
		//if (i == lastBlock)
			//j = lastThread + 1;
		j = 768;
		for (; j < 769; ++j)  // start from the last success point
		{
			resFile = fopen("E:\\matrix\\UIR\\outFuncOptim768.txt", "a");
			//GPUFillRand(a, true);
			//cudaMemcpy(dev_a, a, SIZE * sizeof(float), cudaMemcpyHostToDevice);

			cudaEvent_t start, stop;
			float elapsedTime;

			cudaEventCreate(&start);
			cudaEventCreate(&stop);

			cudaEventRecord(start, 0);

			GPUOutFunc << <i, j >> >(dev_a, dev_c);

			cudaEventRecord(stop, 0);
			cudaEventSynchronize(stop);

			cudaEventElapsedTime(&elapsedTime, start, stop);

			//cudaMemcpy(copy_dev_c, dev_c, SIZE * sizeof(float), cudaMemcpyDeviceToHost);

			if (elapsedTime > 0 && elapsedTime < bestRes.elapsedTime)
			{
				bestRes.blockCount = i;
				bestRes.threadCount = j;
				bestRes.elapsedTime = elapsedTime;
			}

			printf("blockCount = %5d threadCount = %4d elapsedTime = %f\n", i, j, elapsedTime);
			fprintf(resFile, "%5d %6d %16.6f\n", i, j, elapsedTime);
			fclose(resFile);
		}
	}

	cudaFree((void**)&dev_a);
	cudaFree((void**)&dev_c);
	free(a);
	free(c);
	free(copy_dev_c);
}

int main()
{
	optimOutFunc();
	return 0;
}
