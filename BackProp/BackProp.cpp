// BackProp.cpp : Defines the entry point for the console application.
//

#include "Nnetwork.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>


#include <iostream>
#include <fstream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
	std::vector<std::string>   result;
	std::string                line;
	std::getline(str, line);

	std::stringstream          lineStream(line);
	std::string                cell;

	while (std::getline(lineStream, cell, ','))
	{
		result.push_back(cell);
	}
	return result;
}


//inpf - ����� ��������� �����
//col_names - ���� ��������� ����� �������
//data - ���� ��������� ����������������� �������(��. ������)
void read_csv(
	std::ifstream &inpf,
	std::vector<std::string> &col_names,
	std::vector<std::vector<double> > &data)
{
	col_names = getNextLineAndSplitIntoTokens(inpf);

	for (int i = 0; i < col_names.size(); i++)
	{
		data.push_back(std::vector<double>());
	}
	int ncol = col_names.size();
	std::vector<std::string> line;
	while ((line = getNextLineAndSplitIntoTokens(inpf)).size() > 0)
	{
		for (int i = 0; i < ncol; ++i)
		{
			data.at(i).push_back(std::stod(line.at(i)));
		}
	}
}


struct EducationPair
{
	float *input;
	float *output;
};


/*EducationPair *read_csv(const char *fileName, int &eduSize)
{
std::ifstream loadFile;
loadFile.open(fileName, std::ios::in);

std::vector<std::string> colNames;
std::vector<std::vector<double>> data;

read_csv(loadFile, colNames, data);

//Hardcode because of useless data in file
eduSize = data[0].size();

EducationPair *edu = new EducationPair[eduSize];

for (int i = 0; i < eduSize; ++i)
{
edu[i].input = new float[12];

for (int j = 0; j < 11; ++j)
{
edu[i].input[j] = data[j + 3][i];
//std::cout << edu[i].input[j] << std::endl;
}
edu[i].output = new float[1];
edu[i].output[0] = data[14][i];

//std::cout << std::endl << edu[i].output[0] << std::endl << std::endl;
}

loadFile.close();

return edu;
}*/

EducationPair *read_csv(const char *fileName, int &eduSize, int inputSize, int outputSize)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	if (!loadFile.good())
		return NULL;

	std::vector<std::string> colNames;
	std::vector<std::vector<double>> data;

	read_csv(loadFile, colNames, data);

	//Hardcode because of useless data in file
	eduSize = data[0].size();

	EducationPair *edu = new EducationPair[eduSize];

	for (int i = 0; i < eduSize; ++i)
	{
		edu[i].input = new float[inputSize];

		for (int j = 0; j < inputSize; ++j)
		{
			edu[i].input[j] = data[j][i];
			//std::cout << edu[i].input[j] << std::endl;
		}
		//std::cout << std::endl;
		edu[i].output = new float[outputSize];
		for (int j = 0; j < outputSize; ++j)
		{
			edu[i].output[j] = data[j + inputSize][i];
			//std::cout << edu[i].output[j] << std::endl;
		}
		//std::cout << std::endl << edu[i].output[0] << std::endl << std::endl;
		//std::cout << std::endl;
	}

	loadFile.close();

	return edu;
}

//EducationPair *sintezEducationCos(int eduSize, float left, float right)
//{
//	EducationPair *edu = new EducationPair[eduSize];
//
//	float step = (right - left) / eduSize;
//
//	int ind = 0;
//	float pos = left;
//
//	while (pos < right)
//	{
//		edu[ind].input[0] = pos;// (ind * step) / (right - left);
//		edu[ind].input[1] = -1;
//		float x = pos;
//
//		edu[ind].output = (tanh(cos(x *x * x*x))*tanh(abs(0.1*x *x *x - 2))*tanh(2 * cos(x * x) + 2)*tanh(x) + 1) / 2.;// exp(pos) / 10.;// cos(pos);// abs(sin(pos) + abs(cos(pos) + abs(pos)));// cos(pos);
//
//		ind++;
//		pos += step;
//	}
//
//	return edu;
//}

EducationPair *getXorEdu(int &eduSize)
{
	EducationPair *edu = new EducationPair[4];

	for (int i = 0; i < 4; ++i)
	{
		edu[i].input = new float[2];
		edu[i].output = new float[1];
	}

	edu[0].input[0] = 0;
	edu[0].input[1] = 0;
	edu[0].output[0] = 0;

	edu[1].input[0] = 1;
	edu[1].input[1] = 0;
	edu[1].output[0] = 1;

	edu[2].input[0] = 0;
	edu[2].input[1] = 1;
	edu[2].output[0] = 1;

	edu[3].input[0] = 1;
	edu[3].input[1] = 1;
	edu[3].output[0] = 0;

	eduSize = 4;
	return edu;


}
void writeInFile(EducationPair *edu, float **netOutput, int size, char* fileName = "TestOutput.txt")
{
	std::ofstream saveFile;
	saveFile.open(fileName, std::ios::out | std::ios::binary);


	//for (int i = 0; i < size; ++i)
	//{
	//	//saveFile << edu[i].input << std::endl;
	//}

	//saveFile << std::endl << std::endl;

	//for (int i = 0; i < size; ++i)
	//{
	//	saveFile << edu[i].output << std::endl;
	//}

	//saveFile << std::endl << std::endl;

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < 1; ++j)
			saveFile << netOutput[i][j] << ' ';
		saveFile << std::endl;
	}

	saveFile << std::endl << std::endl;

	saveFile.close();
}




int main()
{
	Nnetwork net(4, 25, 3);
	net.init(-1, 1);

	int eduSize = 0;
	//EducationPair *edu = sintezEducationCos(eduSize, -5, 2);
	EducationPair *edu = read_csv("IRIS.txt", eduSize, 4, 3);
	//EducationPair *edu = getXorEdu(eduSize);

	double mistake = 0;
	int epoche = 1;
	float deltaNu = 0.01;
	float bestRes = 0xffff;

	float lastMistake = 0xffffff;

	do
	{
		mistake = 0;
		int eduLim = eduSize;
		for (int i = 0; i < eduSize; ++i)
		{
			int ind = i;// rand() % eduSize;
			net.wave(edu[ind].input);
			double currnetMistake = net.getMistake(edu[ind].output);

			mistake = currnetMistake > mistake ? currnetMistake : mistake;

			//if (currnetMistake < 0.01)
			//{
			//	--i;
			//	eduLim--;
			//	if (eduLim < 0)
			//		break;
			//	continue;
			//}
			net.teachNet(edu[ind].output);
		}

		//std::cout << "epoch " << mistake << std::endl;

		if (epoche > 10000)
			break;
		else
			epoche++;

		/*if (lastMistake <= mistake)
			break;
		else
			lastMistake = mistake;*/
		epoche++;
	} while (mistake > 0.001);

	//delete[] edu;

	//int testEduSize = 100;
	//EducationPair *testEdu = sintezEducationCos(testEduSize, -5, 2);
	//float *netOutput = new float[testEduSize];

	////net.loadState(mistake);

	//for (int i = 0; i < testEduSize; ++i)
	//{
	//	netOutput[i] = *net.wave(testEdu[i].input);
	//}

	//writeInFile(testEdu, netOutput, testEduSize, "PukanPetrovich.txt");

	float **netOutput = new float *[eduSize];

	for (int i = 0; i < eduSize; ++i)
		netOutput[i] = new float[3];

	//net.loadState(mistake);

	for (int i = 0; i < eduSize; ++i)
	{
		float *IRIS = net.wave(edu[i].input);
		netOutput[i][0] = IRIS[0];
		netOutput[i][1] = IRIS[1];
		netOutput[i][2] = IRIS[2];
	}

	writeInFile(edu, netOutput, eduSize, "IRIS_OUT.txt");

	return 0;
}

