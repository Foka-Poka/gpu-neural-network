#pragma once
class Nnetwork
{
public:
	Nnetwork(int input, int neuronCount, int output);
	~Nnetwork();

	float *wave(float *in);
	void init(float left = -1, float right = 1);
	void teachNet(float *recomended);
	double getMistake(const float *recomended);

public:
	int m_input;
	int m_output;
	int m_neuronCount;
	float nu;

	float *deltaOut;
	float *deltaHidden;

	float *outHidden;
	float *X;
	float *Y;

	float **W;
	float **V;

	float *helpVect;
	float kExp;
};

