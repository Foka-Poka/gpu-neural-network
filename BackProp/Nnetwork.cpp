#include "Nnetwork.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

float sigma(float x)
{
	return 1. / (1 + exp(-x));
}

float scolar(float *a, float *b, int size)
{
	long double sum = 0.0;

	for (int i = 0; i < size; ++i)
	{
		sum += a[i] * b[i];
	}

	return sum;
}


Nnetwork::Nnetwork(int input, int neuronCount, int output) :
m_input(input + 1), m_output(output), m_neuronCount(neuronCount + 1)
{
	X = new float[m_input];
	Y = new float[m_output];
	deltaOut = new float[m_output];
	deltaHidden = new float[m_neuronCount];

	outHidden = new float[m_neuronCount];
	helpVect = new float[m_neuronCount * m_output * m_input];

	V = new float *[m_neuronCount];
	for (int i = 0; i < m_neuronCount; ++i)
	{
		V[i] = new float[m_input];
	}

	W = new float *[m_output];
	for (int i = 0; i < m_output; ++i)
	{
		W[i] = new float[m_neuronCount];
	}

	kExp = 0.3;
	nu = 1;
}


Nnetwork::~Nnetwork()
{
	delete[] X;
	delete[] Y;
	delete[] deltaOut;
	delete[] deltaHidden;
	delete[] outHidden;

	delete[] helpVect;

	for (int i = 0; i < m_neuronCount; ++i)
	{
		delete[] V[i];
	}
	delete V;
	V = nullptr;

	for (int i = 0; i < m_output; ++i)
	{
		delete[] W[i];
	}
	delete W;
	W = nullptr;
}

void Nnetwork::init(float min, float max)
{
	float leingth = max - min;

	for (int i = 0; i < m_neuronCount; ++i)
	{
		for (int j = 0; j < m_input; ++j)
		{
			V[i][j] = min + leingth * (float)rand() / RAND_MAX;
		}
	}

	for (int i = 0; i < m_output; ++i)
	{
		for (int j = 0; j < m_neuronCount; ++j)
		{
			W[i][j] = min + leingth * (float)rand() / RAND_MAX;;
		}
	}
}

float* Nnetwork::wave(float *in)
{
	for (int i = 0; i < m_input - 1; ++i)
		X[i] = in[i];
	X[m_input - 1] = -1;

	for (int i = 0; i < m_neuronCount; ++i)
	{
		helpVect[i] = scolar(V[i], X, m_input);
	}

	for (int i = 0; i < m_neuronCount; ++i)
	{
		outHidden[i] = sigma(helpVect[i]);
	}
	outHidden[m_neuronCount - 1] = -1;

	for (int i = 0; i < m_output; ++i)
	{
		helpVect[i] = scolar(W[i], outHidden, m_neuronCount);
	}

	for (int i = 0; i < m_output; ++i)
	{
		Y[i] = sigma(helpVect[i]);
	}

	return Y;
}

void Nnetwork::teachNet(float* T)
{
	for (int i = 0; i < m_output; ++i)
	{
		deltaOut[i] = Y[i] * (1 - Y[i]) * (T[i] - Y[i]);
	}

	for (int i = 0; i < m_neuronCount; ++i)
	{
		float deltaSum = 0;
		
		for (int j = 0; j < m_output; ++j)
		{
			deltaSum += deltaOut[j] * W[j][i];
		}
		
		deltaHidden[i] = outHidden[i] * (1 - outHidden[i]) * deltaSum;
	}

	for (int i = 0; i < m_neuronCount; ++i)
	{
		for (int j = 0; j < m_input; ++j)
		{
			V[i][j] += nu * deltaHidden[i] * X[j];
		}
	}

	for (int i = 0; i < m_output; ++i)
	{
		for (int j = 0; j < m_neuronCount; ++j)
		{
			W[i][j] += nu * deltaOut[i] * outHidden[j];
		}
	}

	nu *= 0.999;
}

double Nnetwork::getMistake(const float *recomended)
{
	double mistake = 0;
	double sum = 0;

	for (int i = 0; i < m_output; ++i)
		sum += (Y[i] - recomended[i]) * (Y[i] - recomended[i]);

	mistake = sum / 2;

	return mistake;
}