#ifndef READ_CSV_H
#define READ_CSV_H

#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>

using namespace std;
/* ПРИМЕР

таблица.csv:
"x","y"
1,2
3,4
5,6

результат:
col_names = ["x", "y"]
data = [[1.0, 2.0, 3.0] , [2.0, 4.0, 6.0]]

*/


// http://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c
std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

	while (std::getline(lineStream, cell, ','))
    {
        result.push_back(cell);
    }
    return result;
}


//inpf - поток файлового ввода
//col_names - сюда запишутся имена колонок
//data - сюда запишется транспонированная таблица(см. пример)
void read_csv(
			std::ifstream &inpf, 
			std::vector<std::string> &col_names, 
			std::vector<std::vector<double> > &data,
			int eduLim = 0xffffff)
{
	col_names = getNextLineAndSplitIntoTokens(inpf);

	for (int i = 0; i < col_names.size(); i++)
	{
		data.push_back(std::vector<double>());
	}
	int ncol = col_names.size();
	std::vector<std::string> line;

	while ((line = getNextLineAndSplitIntoTokens(inpf)).size() > 0) 
	{
		
		for (int i = 0; i < ncol; ++i)
		{
			data.at(i).push_back(std::stod(line.at(i)));
		}
		eduLim--;
		if (eduLim == 0)
			break;
	}
}


struct EducationPair
{
	float *input;
	float *output;
};


/*EducationPair *read_csv(const char *fileName, int &eduSize)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	std::vector<std::string> colNames;
	std::vector<std::vector<double>> data;

	read_csv(loadFile, colNames, data);

	//Hardcode because of useless data in file
	eduSize = data[0].size();

	EducationPair *edu = new EducationPair[eduSize];

	for (int i = 0; i < eduSize; ++i)
	{
		edu[i].input = new float[12];

		for (int j = 0; j < 11; ++j)
		{
			edu[i].input[j] = data[j + 3][i];
			//std::cout << edu[i].input[j] << std::endl;
		}
		edu[i].output = new float[1];
		edu[i].output[0] = data[14][i];

		//std::cout << std::endl << edu[i].output[0] << std::endl << std::endl;
	}

	loadFile.close();

	return edu;
}*/

EducationPair *read_csv(const char *fileName, int &eduSize, int inputSize, int outputSize)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	if (!loadFile.good())
		return NULL;

	std::vector<std::string> colNames;
	std::vector<std::vector<double>> data;

	read_csv(loadFile, colNames, data);

	//Hardcode because of useless data in file
	eduSize = data[0].size();

	EducationPair *edu = new EducationPair[eduSize];

	for (int i = 0; i < eduSize; ++i)
	{
		edu[i].input = new float[inputSize];

		for (int j = 0; j < inputSize; ++j)
		{
			edu[i].input[j] = data[j][i];
			//std::cout << edu[i].input[j] << std::endl;
		}
		//std::cout << std::endl;
		edu[i].output = new float[outputSize];
		for (int j = 0; j < outputSize; ++j)
		{
			edu[i].output[j] = data[j+inputSize][i];
			//std::cout << edu[i].output[j] << std::endl;
		}
		//std::cout << std::endl << edu[i].output[0] << std::endl << std::endl;
		//std::cout << std::endl;
	}

	loadFile.close();

	return edu;
}


EducationPair *getXorEdu(int &eduSize)
{
	EducationPair *edu = new EducationPair[4];

	for (int i = 0; i < 4; ++i)
	{
		edu[i].input = new float[2];
		edu[i].output = new float[1];
	}

	edu[0].input[0] = 0;
	edu[0].input[1] = 0;
	edu[0].output[0] = 0;

	edu[1].input[0] = 1;
	edu[1].input[1] = 0;
	edu[1].output[0] = 1;

	edu[2].input[0] = 0;
	edu[2].input[1] = 1;
	edu[2].output[0] = 1;

	edu[3].input[0] = 1;
	edu[3].input[1] = 1;
	edu[3].output[0] = 0;

	eduSize = 4;
	return edu;
}

EducationPair* getMNIST(int &input, int &output, int &eduSize, char *fileName, int eduLim = 0xffffff)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	if (!loadFile.good())
		return NULL;

	std::vector<std::string> colNames;
	std::vector<std::vector<double>> data;

	read_csv(loadFile, colNames, data, eduLim);

	//Hardcode because of useless data in file
	for (int i = 0; i < colNames.size(); ++i)
		cout << i << ' ' << colNames[i] << endl;
	eduSize = data[0].size();

	output = 10;
	input = colNames.size() - 1;

	EducationPair *edu = new EducationPair[eduSize];

	for (int i = 0; i < eduSize; ++i)
	{
		edu[i].input = new float[input];

		for (int j = 0; j < input; ++j)
		{
			edu[i].input[j] = data[j+1][i];
			//std::cout << edu[i].input[j] << std::endl;
		}
		//std::cout << std::endl;
		edu[i].output = new float[output];

		int a = data[0][i];
		for (int j = 0; j < output; ++j)
		{
			edu[i].output[j] = 0;

			//std::cout << edu[i].output[j] << std::endl;
		}
		edu[i].output[a] = 1;
		//std::cout << std::endl << edu[i].output[0] << std::endl << std::endl;
		//std::cout << std::endl;
	}
	
	loadFile.close();

	return edu;
}

void normalizeMNIST(char *fileName)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	if (!loadFile.good())
		return;

	std::vector<std::string> colNames;
	std::vector<std::vector<double>> data;

	read_csv(loadFile, colNames, data);

	for (int i = 1; i < colNames.size(); ++i)
	{
		for (int j = 0; j < data[0].size(); ++j)
		{
			double a = data[i][j] / 256.;
			data[i][j] = a;
		}
	}

	std::ofstream saveFile;
	saveFile.open("MNIST\\normalaziesMNISTTest.csv", std::ios::out | std::ios::binary);

	for (int i = 0; i < colNames.size(); ++i)
	{
		saveFile << colNames[i] << ',';
	}

	saveFile << std::endl;
	for (int i = 0; i < data[0].size(); ++i)
	{
		for (int j = 0; j < colNames.size(); ++j)
		{
			saveFile << data[j][i] << ',';
		}
		saveFile << std::endl;
	}

	loadFile.close();
	saveFile.close();
}
#endif 