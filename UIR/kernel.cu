
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math_functions.h"
#include "math_constants.h"
#include "cuda.h"
#include "curand.h"

#include "neuronNetwork.h"


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <math.h>


#include "neuronNetworkGPU.h"


#include <iostream>
#include <fstream>

#include "csv_read.h"

using namespace std;

template<class T>
int findMaxInd(T* mas, int size)
{
	int ind = 0;
	T maxRes = -0xffffff;

	for (int i = 0; i < size; ++i)
	{
		if (maxRes < mas[i])
		{
			maxRes = mas[i];
			ind = i;
		}
	}

	return ind;
}

EducationPair *sintezEducationCos(int eduSize, float left, float right)
{
	EducationPair *edu = new EducationPair[eduSize];

	float step = (right - left) / eduSize;

	int ind = 0;
	float pos = left;

	while (pos < right)
	{
		edu[ind].input = new float[1];
		edu[ind].output = new float[1];
		edu[ind].input[0] = (ind * step) / (right - left);
		float x = pos;
		edu[ind].output[0] = cos(x);// cos(x) + cos(2 * x) + cos(3 * x);// (x*x*x*x + 5 * x*x*x - 10 * x);//  cos(pos);// abs(sin(pos) + abs(cos(pos) + abs(pos)));// cos(pos);

		ind++;
		pos += step;
	}

	return edu;
}

void writeInFile(EducationPair *edu, float **netOutput, int size, int outputSize, bool isRecomended = false, char* fileName = "TestOutput.txt")
{
	std::ofstream saveFile;
	saveFile.open(fileName, std::ios::out | std::ios::binary);


	//for (int i = 0; i < size; ++i)
	//{
	//	//saveFile << edu[i].input << std::endl;
	//}

	//saveFile << std::endl << std::endl;

	int currenSize = outputSize;
	outputSize = 1;

	for (int i = 0; i < size && isRecomended; ++i)
	{
		for (int j = 0; j < outputSize; ++j)
			saveFile << edu[i].output[j] << ' '; //saveFile << findMaxInd(edu[i].output, currenSize);//saveFile << edu[i].output[j] << ' ';
		saveFile << endl;
	}

	saveFile << std::endl << std::endl;

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < outputSize; ++j)
			saveFile << netOutput[i][j];// findMaxInd(netOutput[i], currenSize); //saveFile << netOutput[i][j] << ' ';
		saveFile << std::endl;
	}

	saveFile << std::endl << std::endl;

	saveFile.close();
}

void mistakeFile(double *mistake, int size, char* fileName = "Mistake.txt")
{

	std::ofstream saveFile;
	saveFile.open(fileName, std::ios::out | std::ios::binary);

	for (int i = 0; i < size; ++i)
	{
		saveFile << mistake[i] << std::endl;
	}

	saveFile << std::endl << std::endl;

	saveFile.close();
}

int main()
{
	srand(time(NULL));

	int neuronCount = 5;
	int input = 1;
	int output = 1;

	double mistake = 0;
	double bestRes = 0xffff;

	int eduSize = 1000;
	//normalizeMNIST("MNIST\\test_govno.csv");
	//EducationPair *edu = getMNIST(input, output, eduSize, "MNIST\\normalaziesMNISTTrain.csv");

	//NeuronNetwork net("MNIST_FULL_200_fixed.txt");
	NeuronNetwork net(input, neuronCount, output);
	net.setFilePath("FuncCos2.txt");
	net.init(-1./(2 * input), 1./(2 * input));
	//net.loadState(bestRes);


	//net.loadState(bestRes);


	int epoche = 0;
	float nu = 1;
	float deltaNu = 0.001;

	float lastMistake = 0xffffff;

	EducationPair *edu = sintezEducationCos(eduSize, -5, 5);
	//EducationPair *edu = read_csv("Cisco_Net.csv", eduSize);
	//EducationPair *edu = read_csv("IRIS.txt", eduSize, 4, 3);
	//EducationPair *edu = getXorEdu(eduSize);

	//double *mistakeRow = new double[150000];

	nu = 0.5; //1. / input;

	do
	{
		//nu *= 0.99999;
		mistake = 0;
		int eduLim = eduSize;
		for (int i = 0; i < eduSize; ++i)
		{
			int ind = i;// rand() % eduSize;
			net.wave(edu[ind].input);
			double currnetMistake = net.getMistake(edu[ind].output);

			mistake = currnetMistake > mistake ? currnetMistake : mistake;

			/*if (currnetMistake < 0.01)
			{
				--i;
				eduLim--;
				if (eduLim < 0)
					break;
				continue;
			}*/
			net.teachNet(edu[ind].input, edu[ind].output, nu);
		}
	//	//nu -= 0.001;
		if (mistake < bestRes)
		{
			net.saveState(mistake);
			bestRes = mistake;
		}

		/*if (lastMistake > mistake)
		{
			nu += deltaNu;
		}
		else
		{
			nu -= deltaNu;
		}*/

		/*if (mistake > bestRes && (epoche % 30 == 0))
			net.loadState(mistake);*/

		std::cout << "epoch " << epoche << " " << mistake << std::endl;
		//mistakeRow[epoche] = mistake;
		//epoche++;

		if (nu <= 0.000001) break;
		if (epoche > 50000)
			break;
		else
			epoche++;

	//	if (lastMistake == mistake)
	//		break;
	//	else
	//		lastMistake = mistake;
	//		
	} while (mistake > 0.00001);

	////delete[] edu;
	
	int testEduSize = 100;
	EducationPair *testEdu = sintezEducationCos(testEduSize, -5, 5);
	//EducationPair *testEdu = read_csv("IRIS.txt", testEduSize, 4, 3);
	//EducationPair *testEdu = getMNIST(input, output, testEduSize, "MNIST\\normalaziesMNISTTest.csv");

	float **netOutput = new float *[testEduSize];

	for (int i = 0; i < testEduSize; ++i)
		netOutput[i] = new float [output];

	net.loadState(mistake);

	for (int i = 0; i < testEduSize; ++i)
	{
		float *a = net.wave(testEdu[i].input);
		for (int j = 0; j < output; ++j)
			netOutput[i][j] = a[j];
	}

	writeInFile(testEdu, netOutput, testEduSize, output, true, "OUT_Sin.txt");
	//mistakeFile(mistakeRow, epoche - 1);
}