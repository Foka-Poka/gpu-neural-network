#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <math.h>

class NeuronNetwork
{
public:
	NeuronNetwork(int input, int neuronCount, int output);
	NeuronNetwork(char *path);
	~NeuronNetwork();

public:
	float* wave(const float *in, bool isOut = false);
	void init(float min = -1.0f, float max = 1.0f);

	double getMistake(const float *recomended);
	float* getY() { return Y; }
	int getInputSize() { return m_inputSize; }

	double teachNet(const float *input, const float *recomended, float nu);
	
	void saveState(double globalMistake = 0xffffff);
	void loadState(double &mistake);
//private:
	void outFuncH();
	void outFunc();
	void sigma(float *a, float *c, int size);

	float *getDiff(float *a, float *b, int size);

	void learnOutlayer(const float *recomendedOut);
	void learnHiddenLayer(float nu);

	void setV(float *vect);
	void setW(float *vect);
	void changeV(int i, int j, float num) { V[i][j] += num; }

	void setFilePath(char *path) { fileName = path; }


private:
	int m_inputSize;
	int m_neuronCount;
	int m_outputSize;

	float **V;			// �� �������� ���� � ��������
	float **W;			// �� �������� � ������
	float *Y;
	float *X;
	float *outVectorHidden;

	float *deltaOut;
	float *delta1Out;
	float *h_d;
	float *h_out;

	float kExp;
	float *tetta;

	char *fileName;
};