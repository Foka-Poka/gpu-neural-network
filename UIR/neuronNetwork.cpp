#include "neuronNetwork.h"
#include "mathCPU.h"
#include <math.h>
#include <iostream>
#include <fstream>

NeuronNetwork::NeuronNetwork(int input, int neuronCount, int output) :
m_inputSize(input),
m_neuronCount(neuronCount),
m_outputSize(output)
{
	X = new float[m_inputSize + 1];
	Y = new float[m_outputSize];
	deltaOut = new float[m_outputSize];
	delta1Out = new float[m_neuronCount + 1];

	h_out = new float[m_neuronCount + 1];
	h_d = new float[m_neuronCount * m_outputSize * m_inputSize];

	V = new float *[m_neuronCount + 1];
	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		V[i] = new float[m_inputSize+1];
	}

	W = new float *[m_outputSize];
	for (int i = 0; i < m_outputSize; ++i)
	{
		W[i] = new float[m_neuronCount+1];
	}

	tetta = new float[m_neuronCount + 1];
	kExp = 0.3;// 0.5; // 1. / (m_inputSize * m_inputSize);
	

}

NeuronNetwork::NeuronNetwork(char *path) : fileName(path)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	if (!loadFile.good())
	{
		std::cout << "FAIL impossible to open file " << path;
		return;
	}
	double globalMistake;

	loadFile >> globalMistake;
	//std::cout << "global Mistake " << globalMistake << std::endl;
	loadFile >> m_inputSize;
	loadFile >> m_neuronCount;
	loadFile >> m_outputSize;
	loadFile >> kExp;

	X = new float[m_inputSize + 1];
	Y = new float[m_outputSize];
	deltaOut = new float[m_outputSize];
	delta1Out = new float[m_neuronCount + 1];

	h_out = new float[m_neuronCount + 1];
	h_d = new float[m_neuronCount + 1];

	V = new float *[m_neuronCount + 1];
	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		V[i] = new float[m_inputSize + 1];
	}

	W = new float *[m_outputSize];
	for (int i = 0; i < m_outputSize; ++i)
	{
		W[i] = new float[m_neuronCount + 1];
	}

	tetta = new float[m_neuronCount + 1];

	for (int i = 0; i < m_neuronCount; ++i)
		loadFile >> tetta[i];

	for (int j = 0; j < m_inputSize + 1; ++j)
	{
		for (int i = 0; i < m_neuronCount + 1; ++i)
			loadFile >> V[i][j];
	}

	for (int j = 0; j < m_neuronCount + 1; ++j)
	{
		for (int i = 0; i < m_outputSize; ++i)
			loadFile >> W[i][j];
	}
	loadFile.close();

}

NeuronNetwork::~NeuronNetwork()
{
	delete[] X;
	delete[] Y;
	delete[] deltaOut;
	delete[] delta1Out;
	delete[] h_out;

	delete[] h_d;
	delete[] tetta;

	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		delete[] V[i];
	}
	delete V;
	V = nullptr;

	for (int i = 0; i < m_outputSize; ++i)
	{
		delete[] W[i];
	}
	delete W;
	W = nullptr;
}

void NeuronNetwork::init(float min, float max)
{
	float leingth = max - min;

	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		for (int j = 0; j < m_inputSize + 1; ++j)
		{
			V[i][j] = min + leingth * (float)rand() / RAND_MAX;
		}
	}

	for (int i = 0; i < m_outputSize; ++i)
	{
		for (int j = 0; j < m_neuronCount + 1; ++j)
		{
			W[i][j] = min + leingth * (float)rand() / (RAND_MAX * (1 + m_neuronCount)) ;
		}
	}
}

float* NeuronNetwork::wave(const float *in, bool isOut)
{
	//////////////////////////////////////////////////////////////////////////
	///������ �, �� ���� �� �� �������� �� ����, ����� �������////////////////
	for (int i = 0; i < m_inputSize; ++i)
		X[i] = in[i];
	X[m_inputSize] = -1;
	//////////////////////////////////////////////////////////////////////////


	/////1 ����///////////////////////////////////////////////////////////////
	for (int i = 0; i < m_neuronCount; ++i)
	{
		h_d[i] = mathCPU::scolar(X, V[i], m_inputSize + 1);
		//printf("%d) %g\n", i, h_d[i]);
	}
	sigma(h_d, h_out, m_neuronCount);
	h_out[m_neuronCount] = -1;

	/*for (int i = 0; i < m_neuronCount; ++i)
		printf("%g ", h_out[i]);
	printf("\n");
	printf("\n");*/
	///////////////////////////////////////////////////////////////////////////
	//////////////////////////2 ����///////////////////////////////////////////

	for (int i = 0; i < m_outputSize; ++i)
	{
		h_d[i] = mathCPU::scolar(h_out, W[i], m_neuronCount + 1);
	}

	float k = 1;

	for (int i = 0; i < m_outputSize; ++i)
	{
		Y[i] = k * h_d[i];
	}
	
	
	//outFuncV(h_d, Y, m_outputSize); out leayr is not sigma

	////////////////////////////////////////////////////////////////////////////

	if (isOut)
	{
		for (int i = 0; i < m_outputSize; ++i)
		{
			printf("%g ", Y[i]);
		}
		printf("\n");
	}

	return Y;

}

void NeuronNetwork::outFuncH()
{
	//float t = 0.5;

	sigma(h_d, h_out, m_neuronCount);
}

void NeuronNetwork::sigma(float *a, float *c, int size)
{
	float expRes;
	float denum;
	float res;

	for (int i = 0; i < size; ++i)
	{
		expRes = exp(-kExp * a[i]); //whole function; i thinks the best variant, but should check if it's true
		//expRes = exp(-k*a[i]); //function from the INternet

		denum = 1 + expRes;
		res = 1 / denum;

		c[i] = res;

		//printf("outFuncV %d) %g\n", i, c[i]);
	}
}

double NeuronNetwork::getMistake(const float *recomended)
{
	double mistake = 0;
	double sum = 0;

	for (int i = 0; i < m_outputSize; ++i)
		sum += (Y[i] - recomended[i]) * (Y[i] - recomended[i]);

	mistake = sum / m_outputSize;

	return mistake;
}


float* NeuronNetwork::getDiff(float *a, float *b, int size)
{
	float *res = new float[size];

	for (int i = 0; i < size; ++i)
		res[i] = a[i] - b[i];

	return res;
}

void NeuronNetwork::learnOutlayer(const float *recomendedOut)
{
	//TODO: ���������� ��� �������� ������

	float nu =  0.1 / (m_neuronCount);
	float k = 1;

	for (int i = 0; i < m_outputSize; ++i)
	{
		deltaOut[i] = (k) * (recomendedOut[i] - Y[i]);
	}

	//for (int i = 0; i < m_outputSize; ++i)
	//{
	//	for (int j = 0; j < m_neuronCount; ++j)
	//		W[i][j] += nu * deltaOut[i] * h_out[j];
	//}
}

void NeuronNetwork::learnHiddenLayer(float nu)
{
	//static float nu = 1000;// (float)rand() / RAND_MAX;

	float deltaSum = 0;

	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		deltaSum = 0;
		for (int k = 0; k < m_outputSize; ++k)
			deltaSum += W[k][i] * deltaOut[k];

		delta1Out[i] = kExp * h_out[i] * (1 - h_out[i]) * deltaSum; //http://www.aiportal.ru/articles/neural-networks/back-propagation.html
	}

	for (int i = 0; i < m_neuronCount + 1; ++i)
	{
		for (int j = 0; j < m_inputSize + 1; ++j)
			V[i][j] += nu * delta1Out[i] * X[j];
		
		/*if (h_out[i] < 0.000001) 
			tetta[i]+=0.0001;
		else if (h_out[i] > 0.99999) 
			tetta[i]-=0.0001;*/
	}

	float nuOut = nu / (m_neuronCount);

	for (int i = 0; i < m_outputSize; ++i)
	{
		for (int j = 0; j < m_neuronCount + 1; ++j)
			W[i][j] += nuOut * deltaOut[i] * h_out[j];
	}

}

double NeuronNetwork::teachNet(const float *input, const float *recomended, float nu)
{
	//wave(input, false);
	double currentMistake = getMistake(recomended);
	learnOutlayer(recomended);

	learnHiddenLayer(nu);

	return currentMistake;
}

void NeuronNetwork::saveState(double globalMistake)
{
	std::ofstream saveFile;
	saveFile.open(fileName, std::ios::out | std::ios::binary);

	saveFile << globalMistake << std::endl;
	saveFile << m_inputSize << std::endl;
	saveFile << m_neuronCount << std::endl;
	saveFile << m_outputSize << std::endl;

	saveFile << kExp << std::endl;

	for (int i = 0; i < m_neuronCount + 1; ++i)
		saveFile << tetta[i] << ' ';
	saveFile << std::endl;
	saveFile << std::endl;

	for (int j = 0; j < m_inputSize + 1; ++j)
	{
		for (int i = 0; i < m_neuronCount + 1; ++i)
			saveFile << V[i][j] << ' ';
		saveFile << std::endl;
	}

	saveFile << std::endl;

	for (int j = 0; j < m_neuronCount + 1; ++j)
	{
		for (int i = 0; i < m_outputSize; ++i)
			saveFile << W[i][j] << ' ';
		saveFile << std::endl;
	}

	saveFile.close();
}

void NeuronNetwork::loadState(double &mistake)
{
	std::ifstream loadFile;
	loadFile.open(fileName, std::ios::in);

	double globalMistake;
	int input;
	int neuronCount;
	int output;

	loadFile >> mistake;
	//std::cout << "global Mistake " << globalMistake << std::endl;
	loadFile >> input;
	loadFile >> neuronCount;
	loadFile >> output;
	loadFile >> kExp;

	if (input != m_inputSize || neuronCount != m_neuronCount || output != m_outputSize)
	{
		std::cout << "FAIL loading net from file";
		return /*false*/;
	}

	for (int i = 0; i < m_neuronCount + 1; ++i)
		loadFile >> tetta[i];

	for (int j = 0; j < m_inputSize + 1; ++j)
	{
		for (int i = 0; i < m_neuronCount + 1; ++i)
			loadFile >> V[i][j];
	}

	for (int j = 0; j < m_neuronCount + 1; ++j)
	{
		for (int i = 0; i < m_outputSize; ++i)
			loadFile >> W[i][j];
	}
	loadFile.close();
}

void NeuronNetwork::setV(float *vect)
{
	for (int i = 0; i < m_neuronCount; ++i)
	{
		for (int j = 0; j < m_inputSize; ++j)
			V[i][j] = vect[i * m_inputSize + j];
	}
}

void NeuronNetwork::setW(float *vect)
{
	for (int i = 0; i < m_outputSize; ++i)
	{
		for (int j = 0; j < m_neuronCount; ++j)
			W[i][j] = vect[i * m_neuronCount + j];
	}
}
