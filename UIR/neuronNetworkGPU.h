#ifndef neuronNetworkGPU_h
#define neuronNetworkGPU_h

#include "cublas_v2.h"

texture<float> textureV;
texture<float> textureW;

namespace GPUFunc {
	__global__ void callWave(float *V, float *W, int m_inputSize, int m_neuronCount, int m_outputSize,
		float *dev_X, float *dev_Y, int vPitch, int wPitch, float *outVectorHidden, float *helpVector,
		int *checkPoint);

	__global__ void callMistake(float *Y, float *recomended, int size, float *res);

	__global__ void initNum(float *dev_num, int size, float min, float max);

	/*__global__ void callTeachNet(float *Y, float *recomended, float *errorVect, float *linearK, int outSize,
		float *prevOut, float *deltaOut, int prevSize,
		float *W, int w_width, int w_height, int wPitch);*/

	__global__ void from1dTo2d(float *vec1d, float *vec2d, int width, int height, int pitch);

	__global__ void getDeltaOut(float *Y, float *recomended, float *linearK, float *deltaOut, int outSize); // for linear
	__global__ void getHiddenDelta(float *W, int columnCount, int rowCount, int sharedSize, float *deltaOut, float *res);

	__global__ void learnOutLayer(float *W, int columnCount, int rowCount, float *deltaOut, float *h_out);
	__global__ void learnHiddenLayer(float* X, float *V, int columnCount, int rowCount, 
		float *h_out, float *hiddenMistake, float kExp, float *tetta);

	__global__ void matrixOnVector(float *matrix, int rowCount, int columnCount, int sharedSize, int division, const float *vec, float *res);
	void matrixOnVector(cublasHandle_t handle, float *matrix, int width, int height, float * vec, float *res);

	__global__ void transpose(float *matrix, int rowCount, int columnCount, float *res);
	
	__global__ void sigma(float *input, int size, float *res, float k, float *tetta);
	__global__ void linear(float *input, int size, float *res, float k);
	__device__ void setVect(float *vect, int size, float val = 0);
	__global__ void mistake(float *Y, float *recomended, int size, int sharedSize, float *res);

	__device__ void mistakeVect(float * Y, float *recomended, float *res, int size);
	__device__ void syncBlocks(int *checkPoint);
};

class neuronNetworkGPU
{

public:
	neuronNetworkGPU(int inputSize, int neuronCount, int outputSize);
	neuronNetworkGPU(char *FilePath);

	~neuronNetworkGPU();

	__host__ void initMemory();
	__host__ void clearMemory();

	__host__ void initNetState();
	__host__ void initRandomState();

	float* wave(float *input, bool isOutput = false);

	float* getV();
	float* getW();
	float* getX();
	float* getY();
	float *getTetta();
	double getGlobalMistake() { return lastGlobalMistake; }

	int getInputSize() { return m_inputSize; }
	float getMistake(float *recomended);

	void setV(float *vec);
	void setW(float *vec);
	void setX(float *vec);
	void setTetta(float *vec);
	void setFilePath(char *filePath);

	void setBlocksThreads(int blocks, int threads) { m_blocks = blocks; m_threads = threads; }
	void setLastGlobalMistake(double mistake) { lastGlobalMistake = mistake; }

	void move();


	void saveState(double globalMistake = 0xff);
	bool loadState(double &globalMistake);

	void teachNet(float *input, float *recomended);

private:
	void getVectorFormGPU(float *src, float * dist, int size);
	float getLastMistakeDev(float *dev_decomended);

private:
	int m_inputSize;
	int m_neuronCount;
	int m_outputSize;

	float *V;
	float *W;
	float *Y;
	float *dev_Y;
	float *X;
	float *dev_X;

	size_t vPitch;
	size_t wPitch;
	size_t dev_vPitch;
	size_t dev_wPitch;

	float *outVectorHidden; // in general its count should be the same as layer count, but I use 1 layer
	float *deltaOut;
	float *deltaHiddenOut;
	float *helpVector;
	
	float kExp;
	double lastGlobalMistake;

	float lastMistake;
	float *dev_lastMistake;

	float *tetta;
	float *dev_tetta;

	char *m_filePath;

	int *dev_checkPoint;

	int m_blocks;
	int m_threads;

	cublasHandle_t m_handle;
};

#endif