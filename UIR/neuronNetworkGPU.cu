#pragma once

#include "neuronNetworkGPU.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math_functions.h"
#include "math_constants.h"
#include "cuda.h"
#include "curand.h"

#include <iostream>
#include <fstream>

#define BLOKS 16
#define THREADS 768

#define BLOCK_SIZE 63

const int sharedMemory = 12288;


//__device__ int *checkPoint;

neuronNetworkGPU::neuronNetworkGPU(int inputSize, int neuronCount, int outputSize) :
m_inputSize(inputSize),
m_neuronCount(neuronCount),
m_outputSize(outputSize),
m_filePath(nullptr)
{
	initMemory();
	//initNetState();
	setBlocksThreads(BLOKS, THREADS);
}

neuronNetworkGPU::neuronNetworkGPU(char *filePath) : m_filePath(filePath)
{
	setBlocksThreads(BLOKS, THREADS);

	std::ifstream loadFile;
	loadFile.open(m_filePath, std::ios::in | std::ios::binary);

	double globalMistake;

	loadFile >> globalMistake;
	loadFile >> m_inputSize;
	loadFile >> m_neuronCount;
	loadFile >> m_outputSize;

	initMemory();

	loadState(globalMistake);
}

neuronNetworkGPU::~neuronNetworkGPU()
{
}


__host__ void neuronNetworkGPU::initMemory()
{
	////http://stackoverflow.com/questions/5029920/how-to-use-2d-arrays-in-cuda ///////

	cudaError err = cudaSuccess;
	bool ok = true;

	ok = cublasCreate(&m_handle) == cudaSuccess;


	//ok &= cudaMallocPitch((void**)&V, &vPitch, m_inputSize * sizeof(float), m_neuronCount) == cudaSuccess;
	//ok &= cudaMallocPitch((void**)&W, &wPitch, m_neuronCount * sizeof(float), m_outputSize) == cudaSuccess;
	ok &= cudaMalloc((void**)&V, m_inputSize * m_neuronCount * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&W, m_outputSize * m_neuronCount * sizeof(float)) == cudaSuccess;

	//ok &= cudaBindTexture(NULL, textureV, V, m_inputSize * m_neuronCount * sizeof(float)) == cudaSuccess;
	//ok &= cudaBindTexture(NULL, textureW, W, m_outputSize * m_neuronCount * sizeof(float)) == cudaSuccess;

	Y = new float[m_outputSize];
	ok &= cudaMalloc((void**)&dev_Y, m_outputSize * sizeof(float)) == cudaSuccess;

	X = new float[m_inputSize];
	ok &= cudaMalloc((void**)&dev_X, m_inputSize * sizeof(float)) == cudaSuccess;

	ok &= cudaMalloc((void**)&outVectorHidden, m_neuronCount * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&deltaOut, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&deltaHiddenOut, m_neuronCount * sizeof(float)) == cudaSuccess;

	ok &= cudaMalloc((void**)&helpVector, (m_neuronCount > m_outputSize ? m_neuronCount : m_outputSize) * sizeof(float)) == cudaSuccess;

	//latsMistake = new float;
	ok &= cudaMalloc((void**)&dev_lastMistake, sizeof(float)) == cudaSuccess;

	tetta = (float *)malloc(m_neuronCount * sizeof(float));
	ok &= cudaMalloc((void**)&dev_tetta, m_neuronCount * sizeof(float)) == cudaSuccess;
	ok &= cudaMemset(dev_tetta, 0, m_neuronCount * sizeof(float)) == cudaSuccess;
	//GPUFunc::initNamespace();

	/*
	ok &= cudaMalloc((void**)&dev_checkPoint, sizeof(int)) == cudaSuccess;
	ok &= cudaMemset(dev_checkPoint, 0, sizeof(int)) == cudaSuccess;
	*/

	kExp = max(1. / m_inputSize, 0.05);

	if (!ok)
		std::cout << "FAIL: imposible to allocate memory on GPU" << std::endl;
}

__host__ void neuronNetworkGPU::clearMemory()
{
	cudaFree(V);
	cudaFree(W);

	delete[] Y;
	cudaFree(dev_Y);

	delete[] X;
	cudaFree(dev_X);

	cudaFree(outVectorHidden);
	cudaFree(deltaOut);
	cudaFree(deltaHiddenOut);

	cudaFree(helpVector);
	cudaFree(dev_lastMistake);

	cudaFree(dev_tetta);
	delete[] tetta;
}

__host__ void neuronNetworkGPU::initNetState()
{
	float *host_v = new float[m_neuronCount * m_inputSize];
	float *host_w = new float[m_outputSize * m_neuronCount];

	for (int i = 0; i < m_neuronCount * m_inputSize; ++i)
	{
		host_v[i] = i;
	}

	for (int i = 0; i < m_outputSize * m_neuronCount; ++i)
		host_w[i] = i;

	lastMistake = 0.24121993;

	cudaError_t err;
	bool ok = true;

	ok &= cudaMemcpy(V, host_v, m_inputSize * m_neuronCount * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess;

	ok &= cudaMemcpy(W, host_w, m_outputSize * m_neuronCount * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess;

	delete[] host_v;
	delete[] host_w;

	//cudaMemcpy(dev_lastMistake, &lastMistake, sizeof(float), cudaMemcpyHostToDevice);
}

__host__ void neuronNetworkGPU::initRandomState()
{
	curandGenerator_t gen;
	long param = rand();
	cudaError_t err;


	curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen, param);

	curandGenerateUniform(gen, V, m_inputSize * m_neuronCount);
	GPUFunc::initNum << <m_blocks, m_threads >> >(V, m_inputSize * m_neuronCount, -1, 1);

	curandGenerateUniform(gen, W, m_neuronCount * m_outputSize);
	GPUFunc::initNum << <m_blocks, m_threads >> >(W, m_outputSize * m_neuronCount, -0.1, 0.1);
		//-1. / (m_neuronCount * m_neuronCount), 1. / (m_neuronCount * m_neuronCount));
}

float* neuronNetworkGPU::getV()
{
	float *host_v = new float[m_neuronCount * m_inputSize]; // more memory than needed, to save stack
	cudaError_t err = cudaMemcpy(host_v, V, m_neuronCount * m_inputSize * sizeof(float), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess)
		std::cout << "FAIL: getV";
	return host_v;
}

float* neuronNetworkGPU::getW()
{
	//http://stackoverflow.com/questions/8696008/copying-memory-allocated-by-cudamallocpitch ///
	float *host_w = new float[m_neuronCount * m_outputSize]; // more memory than needed, to save stack
	cudaError_t err = cudaMemcpy(host_w, W, m_outputSize * m_neuronCount * sizeof(float), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess)
		std::cout << "FAIL: getW";
	return host_w;
}

float* neuronNetworkGPU::getX()
{
	getVectorFormGPU(dev_X, X, m_inputSize);
	return X;
}

float* neuronNetworkGPU::getY()
{
	//if (Y != nullptr) delete[] Y;

	getVectorFormGPU(dev_Y, Y, m_outputSize);

	return Y;
}

float neuronNetworkGPU::getMistake(float *recomended)
{
	float *dev_recomended;

	cudaMalloc((void**)&dev_recomended, m_outputSize * sizeof(float));
	cudaMemcpy(dev_recomended, recomended, m_outputSize * sizeof(float), cudaMemcpyHostToDevice);

	getLastMistakeDev(dev_recomended);

	cudaFree(dev_recomended);
	lastMistake /= m_outputSize;
	return lastMistake;
}

float neuronNetworkGPU::getLastMistakeDev(float *dev_recomended)
{
	cudaMemset(dev_lastMistake, 0.0, sizeof(float));

	GPUFunc::mistake << <m_blocks, m_threads, (m_threads + 1) * sizeof(float) >> >(dev_Y, dev_recomended, m_outputSize, m_threads, dev_lastMistake);

	getVectorFormGPU(dev_lastMistake, &lastMistake, 1);

	return lastMistake;
}

float *neuronNetworkGPU::getTetta()
{
	cudaError_t err = cudaMemcpy(tetta, dev_tetta, sizeof(float)* m_neuronCount, cudaMemcpyDeviceToHost);
	return tetta;
}

void neuronNetworkGPU::getVectorFormGPU(float *src, float *dist, int size)
{
	//float *host_vec;
	//host_vec = (float *)malloc(size * sizeof(float));

	cudaError_t err = cudaMemcpy(dist, src, sizeof(float)* size, cudaMemcpyDeviceToHost);
}

void neuronNetworkGPU::setV(float *vec)
{
	cudaError_t err;

	err = cudaMemcpy(V, vec, m_inputSize * m_neuronCount * sizeof(float), cudaMemcpyHostToDevice);
}

void neuronNetworkGPU::setW(float *vec)
{
	cudaError_t err;

	err = cudaMemcpy(W, vec, m_outputSize * m_neuronCount * sizeof(float), cudaMemcpyHostToDevice);
}

void neuronNetworkGPU::setX(float *vec)
{
	cudaError_t err = cudaMemcpy(dev_X, vec, sizeof(float)* m_inputSize, cudaMemcpyHostToDevice);
	if (err != cudaSuccess)
		std::cout << "FAIL: setX" << std::endl;
}

void neuronNetworkGPU::setTetta(float *vec)
{
	cudaError_t err = cudaMemcpy(dev_tetta, vec, sizeof(float)* m_neuronCount, cudaMemcpyHostToDevice);
	if (err != cudaSuccess)
		std::cout << "FAIL: setTetta" << std::endl;
}

void neuronNetworkGPU::setFilePath(char *filePath)
{
	//if (m_filePath != nullptr) delete[] m_filePath;

	m_filePath = filePath;
}

void neuronNetworkGPU::saveState(double globalMistake)
{
	if (m_filePath == NULL)
	{
		std::cout << "fail: path is empty" << std::endl;
		return;
	}

	std::ofstream saveFile;
	saveFile.open(m_filePath, std::ios::out | std::ios::binary);

	saveFile << globalMistake << std::endl;
	saveFile << m_inputSize << std::endl;
	saveFile << m_neuronCount << std::endl;
	saveFile << m_outputSize << std::endl;
	saveFile << kExp << std::endl;

	getTetta();
	for (int i = 0; i < m_neuronCount; ++i)
		saveFile << tetta[i] << ' ';
	saveFile << std::endl;
	saveFile << std::endl;

	float *host_V = getV();
	float *host_W = getW();

	for (int i = 0; i < m_neuronCount; ++i)
	{
		for (int j = 0; j < m_inputSize; ++j)
			saveFile << host_V[j * m_neuronCount + i] << ' ';
		saveFile << std::endl;
	}
	delete[] host_V;

	saveFile << std::endl;

	for (int i = 0; i < m_outputSize; ++i)
	{
		for (int j = 0; j < m_neuronCount; ++j)
			saveFile << host_W[j * m_outputSize + i] << ' ';
		saveFile << std::endl;
	}
	delete[] host_W;

	saveFile.close();
}

bool neuronNetworkGPU::loadState(double &globalMistake)
{
	if (m_filePath == NULL)
	{
		std::cout << "fail: path is empty" << std::endl;
		return false;
	}

	std::ifstream loadFile;
	loadFile.open(m_filePath, std::ios::in | std::ios::binary);

	int inputSize, neuronCount, outputSize;
	float *host_V, *host_W;

	loadFile >> globalMistake;
	loadFile >> inputSize;
	loadFile >> neuronCount;
	loadFile >> outputSize;
	loadFile >> kExp;

	if ((inputSize != m_inputSize) || (neuronCount != m_neuronCount) || (outputSize != m_outputSize))
	{
		std::cout << "FAIL: loaded network differs form current" << std::endl;
		loadFile.close();
		return false;
	}

	for (int i = 0; i < m_neuronCount; ++i)
		loadFile >> tetta[i];

	setTetta(tetta);

	host_V = new float[m_neuronCount * m_inputSize];
	host_W = new float[m_outputSize * m_neuronCount];

	for (int i = 0; i < m_neuronCount; ++i)
	{
		for (int j = 0; j < m_inputSize; ++j)
			loadFile >> host_V[j * m_neuronCount + i];
	}

	setV(host_V);
	delete[] host_V;

	for (int i = 0; i < m_outputSize; ++i)
	{
		for (int j = 0; j < m_neuronCount; ++j)
			loadFile >> host_W[j * m_outputSize + i];
	}

	setW(host_W);

	delete[] host_W;

	loadFile.close();

	return true;
}

#include "mathCPU.h"

float* neuronNetworkGPU::wave(float* input, bool isOutput)
{
	setX(input);

	bool ok = true;
	ok &= cudaMemset(dev_Y, 0.0, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMemset(outVectorHidden, 0.0, m_neuronCount * sizeof(float)) == cudaSuccess;
	ok &= cudaMemset(helpVector, 0, m_neuronCount * sizeof(float)) == cudaSuccess;

	float *host_helpVector = new float[m_neuronCount];
	float *host_res = new float[m_neuronCount];

	///float *waveFront just help vector for intermediate results

	int waveFronSize = m_neuronCount > m_outputSize ? m_neuronCount : m_outputSize;

	int size = m_neuronCount * m_inputSize;
	int blockData = 1 + (float)size / m_blocks;
	int sharedSize = 2 + (float)blockData / m_inputSize;
	int division = sharedMemory / sharedSize;

	division = division > (m_inputSize / 3.) ? (m_inputSize / 3) : division;

	//GPUFunc::matrixOnVector << <m_blocks, m_threads, division * sharedSize * sizeof(float) >> > // ��� �������
	//	(V, m_neuronCount, m_inputSize, sharedSize, division, dev_X, helpVector);

	GPUFunc::matrixOnVector(m_handle, V, m_inputSize, m_neuronCount, dev_X, helpVector);

	//host_helpVector = getVectorFormGPU(helpVector, m_neuronCount); // ������!

	/*for (int i = 0; i < m_neuronCount; ++i)
		std::cout << i << ") " <<host_helpVector[i] << std::endl;*/

	static float lastKExp = -1;
	static float lastTetta = -1;
	static float lastErr = -1;

	GPUFunc::sigma << <m_blocks, m_threads >> >(helpVector, m_neuronCount, outVectorHidden, kExp, dev_tetta);


	cudaMemset(helpVector, 0, waveFronSize * sizeof(float));

	size = m_neuronCount * m_outputSize;
	blockData = 1 + (float)size / m_blocks;
	sharedSize = 2 + (float)blockData / m_neuronCount;

	division = sharedMemory / sharedSize;
	division = division > (m_neuronCount / 3.) ? (m_neuronCount / 3) : division;

	//GPUFunc::matrixOnVector << <m_blocks, m_threads, division * sharedSize * sizeof(float) >> >
	//	(W, m_outputSize, m_neuronCount, sharedSize, division, outVectorHidden, helpVector);

	GPUFunc::matrixOnVector(m_handle, W, m_neuronCount, m_outputSize, outVectorHidden, helpVector);

	GPUFunc::linear << <m_blocks, m_threads >> >(helpVector, m_outputSize, dev_Y, 1);
	cudaMemset(helpVector, 0, waveFronSize);

	//float *h_out = getVectorFormGPU(outVectorHidden, m_neuronCount);


	/*for (int i = 0; i < m_neuronCount; ++i)
		std::cout << i << ") " << h_out[i] << std::endl;

		std::cout << std::endl;

		for (int i = 0; i < m_inputSize; ++i)
		std::cout << host_x[i] << " ";*/

	delete[] host_helpVector;
	delete[] host_res;

	if (isOutput)
	{
		float *host_y = getY();
		for (int i = 0; i < m_outputSize; ++i)
			std::cout << host_y[i] << " ";
		std::cout << std::endl;
		std::cout << std::endl;


		return host_y;
	}


	return nullptr;
}

void neuronNetworkGPU::move()
{
	//kExp *= 0.75;
}

void neuronNetworkGPU::teachNet(float *input, float *recomended)
{
	float currentMistake;

	float *dev_recomended;
	float *errorVect;
	float *dev_linearK, *linearK;
	float *dev_transposeW;

	linearK = new float[m_outputSize];

	for (int i = 0; i < m_outputSize; ++i)
		linearK[i] = 1;

	bool ok = true;
	cudaError_t err;
	ok &= cudaMalloc((void**)&dev_recomended, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&errorVect, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&dev_linearK, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMalloc((void**)&dev_transposeW, m_neuronCount * m_outputSize * sizeof(float)) == cudaSuccess;

	ok &= cudaMemcpy(dev_recomended, recomended, m_outputSize * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess;
	ok &= cudaMemcpy(dev_linearK, linearK, m_outputSize * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess;
	ok &= cudaMemset(errorVect, 0, m_outputSize * sizeof(float)) == cudaSuccess;
	ok &= cudaMemset(deltaHiddenOut, 0, m_outputSize * sizeof(float)) == cudaSuccess;

	if (!ok)
		std::cout << "FAIL: teach net memmory fail" << std::endl;

	//float *hostK = getVectorFormGPU(dev_linearK, m_outputSize);

	//while (currentMistake > teachMistake)
	{
		wave(input);
		currentMistake = getLastMistakeDev(dev_recomended);

		//float *_hostX = getX();


		GPUFunc::getDeltaOut << <m_blocks, m_threads >> > (dev_Y, dev_recomended, dev_linearK, deltaOut, m_outputSize);
		GPUFunc::learnOutLayer << <m_blocks, m_threads >> >(W, m_neuronCount, m_outputSize, deltaOut, outVectorHidden);


		//transpose W
		GPUFunc::transpose << <m_blocks, m_threads >> > (W, m_outputSize, m_neuronCount, dev_transposeW);

		int size = m_neuronCount * m_outputSize;
		int blockData = 1 + (float)size / m_blocks;
		int sharedSize = 2 + (float)blockData / m_neuronCount;
		int division = sharedMemory / sharedSize;

		division = division > (m_inputSize / 3.) ? (m_inputSize / 3) : division;

		cudaMemset(deltaHiddenOut, 0, m_neuronCount * sizeof(float));

		/*GPUFunc::matrixOnVector << <m_blocks, m_threads, division * sharedSize * sizeof(float) >> >
			(W, m_outputSize, m_neuronCount, sharedSize, division, deltaOut, deltaHiddenOut);*/

		GPUFunc::matrixOnVector(m_handle, dev_transposeW, m_outputSize, m_neuronCount, deltaOut, deltaHiddenOut);

		GPUFunc::learnHiddenLayer << <m_blocks, m_threads >> >(dev_X, V, m_inputSize, m_neuronCount, outVectorHidden, deltaHiddenOut, kExp, dev_tetta);
	}

	getTetta();

	delete[] linearK;

	cudaFree(dev_recomended);
	cudaFree(dev_linearK);
	cudaFree(errorVect);
	cudaFree(dev_transposeW);
}


/////// help functions ! //////
///////////////////////////////
///////////////////////////////

//__device__ void exst

__global__ void GPUFunc::transpose(float *matrix, int rowCount, int columnCount, float *res)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	int size = rowCount * columnCount;
	int row = 0;
	int column = 0;

	while (ind < size)
	{
		column = ind / rowCount;
		row = ind % rowCount;

		res[row * columnCount + column] = matrix[ind];
		ind += round;
	}
}

__global__ void GPUFunc::matrixOnVector(float *matrix, int rowCount, int columnCount, int sharedSize, int division, const float *vec, float *res)
{
	int size = rowCount * columnCount;
	int blockData = 1 + (float)size / gridDim.x;
	//int sharedSize = 1 + (float)blockData / columnCount;


	extern __shared__ float s[];

	for (int i = threadIdx.x; i < division * sharedSize;)
	{
		s[i] = 0;
		i += blockDim.x;
	}

	__syncthreads();

	//float *localData = (float*)&s[sharedSize];
	//float localData[600];

	int startPoint = blockIdx.x * blockData;
	int startRow = startPoint / columnCount;


	int ind = startPoint + threadIdx.x;
	int step = blockDim.x;
	int tid = threadIdx.x;


	int limit = min(startPoint + blockData, size);

	int column;// = ind % columnCount;
	int row = ind / columnCount;//__fdividef(ind, columnCount);
	int currentRow = row;
	float elem = 0;

	bool q = false;
	int sharedBlock = threadIdx.x % division;
	int posInd = sharedSize * sharedBlock;

	while (ind < limit)
	{
		q = true;
		column = ind % columnCount;
		row = __fdividef(ind, columnCount);


		//localData[row - startRow] += elem;

		//s[row - startRow] += elem;
		if (row != currentRow)
		{
			q = false;
			int currendIndToCheck = currentRow - startRow + posInd;
			atomicAdd(&s[currentRow - startRow + posInd], elem);
			///atomicAdd(&res[currentRow], elem); // correct
			elem = 0;
			currentRow = row;
			float pukan = currendIndToCheck;
		}

		elem += matrix[ind] * vec[column];

		//atomicAdd(&res[row], elem);
		//localData[tid] = matrix[ind];
		ind += step;

	}

	if (q)
		// atomicAdd(&res[row], elem);//correct
		atomicAdd(&s[row - startRow + posInd], elem);

	__syncthreads();

	int blockLim;
	blockLim = min(size - startPoint, sharedSize);
	blockLim = min(blockLim, rowCount - startRow);

	float localSum = 0;
	for (int i = tid; i < blockLim;)
	{
		localSum = 0;

		for (int j = 0; j < division; ++j)
		{
			localSum += s[i + j * sharedSize];
		}
		//atomicAdd(&res[startRow + i], localData[i]);
		atomicAdd(&res[startRow + i], localSum);
		i += blockDim.x;
	}

}

void GPUFunc::matrixOnVector(cublasHandle_t handle, float *matrix, int width, int height, float *vec, float *res)
{
	cudaError_t cudaStat;
	cublasStatus_t stat;

	//stat = cublasSetMatrix(height, width, sizeof(float),)

	float al = 1.0f;
	float bet = 1.0f;
	stat = cublasSgemv(handle, CUBLAS_OP_N, height, width, &al, matrix, height, vec, 1, &bet, res, 1);

	if (stat != CUBLAS_STATUS_SUCCESS)
		std::cout << "FAIL matrix on vector CUBLAS";
}



__global__ void GPUFunc::sigma(float *input, int size, float *res, float k, float *tetta)
{
	//setVect(tetta, size, 5.0);

	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	while (ind < size)
	{
		float expRes = expf(-tetta[ind] - k * input[ind]);
		float denum = 1 + expRes;
		float sigma = 1 / denum;
		//float k = fdividef(1., t);
		res[ind] = sigma;// min(sigma, 0.999);
		ind += round;
	}

	//setVect << <m_blocks, m_threads >> >(helpVector, waveFronSize, 0);
}

__global__ void GPUFunc::linear(float *input, int size, float *res, float k)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	while (ind < size)
	{
		res[ind] = input[ind] * k;
		ind += round;
	}
}

__device__ void GPUFunc::setVect(float* vect, int size, float val)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	while (ind < size)
	{
		vect[ind] = val;
		ind += round;
	}
}

__global__ void GPUFunc::mistake(float *Y, float *recomended, int size, int sharedSize, float *res) // ����������
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	extern __shared__ float localSum[];

	GPUFunc::setVect(localSum, sharedSize);

	localSum[threadIdx.x] = 0;

	float sum = 0;
	float sqrSum;

	while (ind < size)
	{
		sqrSum = Y[ind] - recomended[ind];
		sum += sqrSum * sqrSum;

		ind += round;
	}

	localSum[threadIdx.x] = sum;

	__syncthreads();

	int width = 1 + size / 2;

	while (width > 2)
	{
		if (threadIdx.x < width)
		{
			float puka = threadIdx.x + width < sharedSize ? localSum[threadIdx.x + width] : 0;
			localSum[threadIdx.x] += puka;
			localSum[threadIdx.x + width] = 0;
		}
		else
			break;

		width = 1 + width / 2;
	}

	__syncthreads();

	if (threadIdx.x == 0)
	{
		float mSum = 0;
		int limit = min(size, 2 * width);

		for (int i = 0; i < limit; ++i)
			mSum += localSum[i];
		atomicAdd(&res[0], mSum);
	}
}

__device__ void GPUFunc::mistakeVect(float * Y, float *recomended, float *res, int size)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	while (ind < size)
	{
		//double
		res[ind] = recomended[ind] - Y[ind];
		ind += round;
	}
}

__global__ void GPUFunc::getDeltaOut(float *Y, float *recomended, float *linearK, float *deltaOut, int outSize)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	while (ind < outSize)
	{
		deltaOut[ind] = linearK[ind] * (recomended[ind] - Y[ind]);
		ind += round;
	}
}

/*__global__ void GPUFunc::getHiddenDelta(float *W, int columnCount, int rowCount, int sharedSize, float *deltaOut, float *res) // ���������� !! �� 1 ������� � ������!!!! �����!

{
int ind = threadIdx.x + blockIdx.x * blockDim.x;
int round = blockDim.x * gridDim.x;

extern __shared__ float localSum[]; //m_threads, clear it!!

int size = columnCount * rowCount;

float sum = 0;
int row = 0;
int column = 0;
while (ind < size)
{
row = ind / columnCount;
sum += deltaOut[row] * W[ind];
}

}*/

__global__ void GPUFunc::learnOutLayer(float *W, int columnCount, int rowCount, float *deltaOut, float *h_out)
{
	float nu = 0.1 / (columnCount);

	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	int size = columnCount * rowCount;
	int row = 0;
	int column = 0;

	while (ind < size)
	{
		row = ind % rowCount;
		column = ind / rowCount;

		//row = __fdividef(ind, columnCount);

		W[ind] += nu * deltaOut[row] * h_out[column];

		ind += round;
	}
}

__global__ void GPUFunc::learnHiddenLayer(float* X, float *V, int columnCount, int rowCount, 
	float *h_out, float *hiddenMistake, float kExp, float *tetta)
{
	float nu = 0.1;// 0.5;// max(0.05, 1. / columnCount);// 0.5;

	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;

	int size = columnCount * rowCount;
	int row = 0;
	int column = 0;

	while (ind < size)
	{
		row = ind % rowCount;
		column = ind / rowCount;

		//row = __fdividef(ind, columnCount);
		float curRes = nu * kExp * hiddenMistake[row] * h_out[row] * (1. - h_out[row]) * X[column];
		V[ind] += curRes;

		if (h_out[row] < 0.00001)
			tetta[row] += 0.00001;
		else if (h_out[row] > 0.9999)
			tetta[row] -= 0.00001;

		ind += round;
	}
}

/*__global__ void GPUFunc::callTeachNet(float *Y, float *recomended, float *errorVect, float *linearK, int outSize,
	float *prevOut, float *deltaOut, int prevSize,
	float *W, int w_width, int w_height, int wPitch, int *checkPoint)
	{
	lernOutLayer(Y, recomended, errorVect, linearK, outSize,
	prevOut, deltaOut, prevSize,
	W, w_width, w_height, wPitch, checkPoint);

	GPUFunc::syncBlocks(checkPoint);
	}*/

__global__ void GPUFunc::from1dTo2d(float *vec1d, float *vec2d, int width, int height, int pitch)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int round = blockDim.x * gridDim.x;
	int size = width * height;

	int row;
	int column;
	float *currentPoint;

	while (ind < size)
	{
		row = ind / width;
		column = ind % width;
		currentPoint = ((float*)((char*)vec2d + row * pitch) + column);

		*currentPoint = vec1d[ind];

		ind += round;
	}
}

__device__ void wait(int *checkPoint)
{
	do {
		bool q = checkPoint[0] == 2;
		if (q) break;
	} while (1);
}

__device__ void GPUFunc::syncBlocks(int *checkPoint)
{
	__syncthreads();
	int *pukan = checkPoint;
	if (threadIdx.x == 0)
		atomicAdd(&checkPoint[0], 1);

	if (threadIdx.x == 0)
	{
		wait(checkPoint);
	}

	int ind = threadIdx.x + blockIdx.x;


	if (ind == 0)
	{
		atomicAdd(&checkPoint[0], -checkPoint[0]);
	}

	__syncthreads();
}

__global__ void GPUFunc::initNum(float *dev_num, int size, float min, float max)
{
	int ind = threadIdx.x + blockIdx.x * blockDim.x;
	int step = blockDim.x * gridDim.x;

	float length = max - min;

	while (ind < size)
	{
		dev_num[ind] = dev_num[ind] * length + min;

		ind += step;
	}
}

